package fr.islandswars.hub;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * File <b>Hub</b> located on fr.islandswars.hub
 * Hub is a part of Islands Wars - Hub.
 * <p>
 * Copyright (c) 2017 Islands Wars.
 *
 * @author xharos, {@literal <xharos@islandswars.fr>}
 * Created the 31/12/2017 at 10:11
 * @since 0.0.1
 */
public class Hub extends JavaPlugin {

    private static Hub instance;

    public Hub() {
        instance = this;
    }

    public static Hub getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {

    }

    @Override
    public void onDisable() {

    }

    @Override
    public void onEnable() {

    }
}
